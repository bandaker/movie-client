ListView = Backbone.View.extend({    
    el: '#list',

    template: Handlebars.compile( $("#list-template").html() ),

    initialize: function(opt) {
        this.model = opt.model;
        this.listenTo(this.model, 'results', this.render);

    }
});