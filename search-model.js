SearchModel = Backbone.Model.extend({
    url: function () {
        return 'http://www.omdbapi.com/?' + this.toQuery()
    },

    toQuery: function () {
        var year = this.get('year') ? '&y=' + this.get('year') : '';
        var search =  's=' + this.get('search') + year;

        return [year,search].join('&');
    },

    parse: function (resp) {
        this.set('results', resp["Search"]);
        this.trigger('results');
    }
});