Backbone.View.prototype.render = function () {
    this.$el.html(this.template(this.model.toJSON()));
};