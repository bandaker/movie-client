SearchView = Backbone.View.extend({
        el: '#search-view',

    events: {
        'submit form': 'search'
    },

    template: Handlebars.compile( $("#search-template").html() ),

    search: function (e) {
        e.preventDefault();
        this.model.set('search', $('#search-text').val());
        this.model.set('year', $('#year').val());
        this.model.fetch();
    }
});