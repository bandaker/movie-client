$(document).on('ready', function () {
    var searchModel = new SearchModel();

    var searchView = new SearchView({
        model: searchModel
    });
    searchView.render();

    var listView = new ListView({
        model: searchModel
    });
    listView.render();
});